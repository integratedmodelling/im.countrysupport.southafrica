worldview im;

@test(
    name = "western_cape_landcover_and_extent_accounts",
    description  = "",
    // add to the list any concepts to be observed in the context below
    observations = ("im|im.countrysupport.southafrica/tests/western_cape_landcover_and_extent_accounts.land-cover-type-resolver",
    				//"southafrica.landcoveraccounts.landcover_additions_reductions_southafrica",
   					//"southafrica.landcoveraccounts.landcover_change_pairwise_southafrica",
    				"southafrica.extentaccount.extents_additions_reductions_southafrica"
    				"southafrica.extentaccount.extents_change_pairwise_southafrica"
    ),
    // add assertions to check the observations after execution
    assertions = ()
)
// change the concept to the one you want; use a URN optionally (in a separate test project including the resource)
observe earth:Region named southafrica_wc_100mt_4326
     over space(urn='local:alessio.bulckaen:im.countrysupport.southafrica:im-nca-southafrica_gadm_4326_western.cape',
	       grid= '100 m',
	       projection='EPSG:4326'),
	      time(start=1990, end=2016, step=5.year);
//		  time(year=2015);
// add any models here

//Annotation using LCT4 map - implies gathering more detailed T4 classes under a same macro-class
model 'local:alessio.bulckaen:im.countrysupport.southafrica:im-data-global-landcover.southafrica_sanbi_lc_4326_001dg_1990',
	  'local:alessio.bulckaen:im.countrysupport.southafrica:im-data-global-landcover.southafrica_sanbi_lc_4326_001dg_2014'
//    'local:alessio.bulckaen:im.countrysupport.southafrica:im-nca-southafrica:landcover_4148_001dg_1990',
//	  'local:alessio.bulckaen:im.countrysupport.southafrica:im-nca-southafrica:landcover_4148_001dg_2014'
//	  local:alessio.bulckaen:im.countrysupport.southafrica:im-nca-sa.lc_4326_001dg_1990,
//	  local:alessio.bulckaen:im.countrysupport.southafrica:im-nca-sa.lc_4326_001dg_2014
	as landcover:LandCoverType classified into
		 landcover:EvergreenBroadleafForest if 1111,
		 landcover:OpenSavanna if 1112,
		 landcover:ClosedSavanna if 1113,
		 landcover:SparseShrubCover if 1114,
		 landcover:EvergreenShrubland if 1115,
		 landcover:Grassland if 1116,
		 landcover:BareArea if 1117,
		 soil:Erodible landcover:BareArea if 1118,
		 landcover:NonIrrigatedArableLand if in (2111 2112 2113),
		 agriculture:Pineapple landcover:PermanentlyIrrigatedArableLand if 2114,
		 landcover:PermanentlyIrrigatedArableLand if in (2121 2122 2123), 
		 agriculture:SugarCane landcover:PermanentlyIrrigatedArableLand if in (2131 2142), 
		 agriculture:SugarCane landcover:NonIrrigatedArableLand if in (2132 2133 2134 2141),
//		 agriculture:SugarCane landcover:InactiveAgriculturalLand if in (2134 2142),			 
		 not economics:ForProfit landcover:NonIrrigatedArableLand if in (2211 2212 2213),
		 landcover:FruitAndBerryPlantation if in (2311 2312 2313),
		 landcover:Vineyard if in (2321 2322 2323),
	     landcover:ForestPlantation if in (2411 2412 2413),
	     landcover:SportLeisureFacility if in (3101 3102 3103 3104 3191),
	     landcover:IndustrialCommercialUnits if in (3111 3121),
	     landcover:HighDensityUrban if in (3151 3152 3153 3154),
	     not policy:Compliant landcover:HighDensityUrban if 3161 to 3164,
	     landcover:MediumDensityUrban if in (3131 3132 3133 3134 3141 3142 3143 3144),
	     landcover:LowDensityUrban if in (3171 3172 3173 3174 3184),
	     landcover:HeterogeneousAgriculturalLand if in (3181 3182 3183),    
	     landcover:MineralExtraction if in (3211 3212 3213 3214 3215),
	     landcover:Wetland if 4111,
	     landcover:WaterBody if 4112,
//Added seasonal waterbodies
         hydrology:Seasonal landcover:WaterBody if 4113;